(ns ^:figwheel-hooks infopalette.aragon.core
  (:require [infopalette.aragon.canvas.gui :as cg]
            [infopalette.aragon.canvas.interaction]
            [infopalette.aragon.canvas.keys-input]
            [reagent.core :as reagent]
            [reagent.dom :as rdom]
            [re-frame.core :as rf]))

(defn render
  []
  (rdom/render [cg/canvas]
               (js/document.getElementById "app")))

(defn ^:after-load clear-cache-and-render!
  []
  (rf/clear-subscription-cache!)
  (render))

(defonce init
         (do
           (rf/dispatch-sync [:interact/activate-keydown-listener])
           (rf/dispatch-sync [:app/start-up])
           (render)))
