(ns infopalette.aragon.defaults)

(defonce font-settings {:font         "sans-serif"
                        :shape-inside "auto"
                        :text-align   "left"
                        :font-size    "1em"})
