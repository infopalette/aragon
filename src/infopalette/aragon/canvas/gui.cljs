(ns infopalette.aragon.canvas.gui
  (:require [infopalette.aragon.debug :as dbg]
            [infopalette.aragon.canvas.location]
            [infopalette.aragon.canvas.keys-input]
            [infopalette.aragon.canvas.resources :as cr]
            [infopalette.aragon.canvas.interaction :as ci]
            [re-frame.core :as rf]
            [reagent.core :as r]
            [reagent.dom :as rdom]
            [clojure.repl :refer [doc]]))

(defn canvas []
  (let [s (r/atom {:interactive-canvas {:status :on}
                   :palette            {:status :off}})]
    (fn []
      (let [db (rf/subscribe [:store/contents])
            key-register (rf/subscribe [:key-event/register])
            mouse-register (rf/subscribe [:mouse-event/register])
            data-register (rf/subscribe [:data-view/register])
            {:keys [width height]} @(rf/subscribe [:browser/canvas-size])]
        [:svg {:key      "interactive-canvas"
               :width    width
               :height   height
               :style    {:background-color :bisque}
               :on-click #(when (= (get-in @s [:palette :status]) :off)
                            (do
                              (rf/dispatch-sync [:interact/mouse-click {:x (.-clientX %)
                                                                        :y (.-clientY %)}])
                              (swap! s assoc-in [:palette :status] :on)))}
         #_(when dbg/debug)
         [cr/rectangle {:key    "vlak"
                        :x      0
                        :y      0
                        :width  (quot width 2)
                        :height (quot height 2)
                        :fill   :coral}]
         [cr/text-in-tspan @db {:width width :y 200}]
         [cr/text-in-tspan @mouse-register {:width width :fill "blue"}]
         [cr/text-in-tspan @key-register {:width width :y 50 :fill "blue"}]
         [cr/text-in-tspan @data-register {:width width :y 400 :fill "blue"}]
         [:text {:x    0
                 :y    (- height 80)
                 :fill :red} @s]
         (when (= (get-in @s [:palette :status]) :on)
           [cr/palette])]))))

