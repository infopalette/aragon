(ns infopalette.aragon.canvas.interaction
  (:require
    [infopalette.aragon.functions :as fns]
    [infopalette.aragon.rdf.data-definition :as dd]
    [infopalette.aragon.rdf.minter :as mint]
    [re-frame.core :as rf]
    [reagent.core :as r]
    [goog.events :as ev]
    [goog.events.EventType :as evt]
    [goog.events.KeyCodes :as evk]))

(defonce mouse-events (r/atom {:interact {:user-input {:mouse {}}}}))

(defonce data-view (r/atom {:data-view {:data-display {}}}))

(rf/reg-fx
  :data-view/global-view-state
  (fn [data]
    (swap! data-view assoc :data-view data)))

(rf/reg-fx
  :interact/global-mouse-state
  (fn [data]
    (swap! mouse-events assoc :interact data)))

(rf/reg-event-fx
  :interact/mouse-click
  (fn [cofx [_ click-coords]]
    (assoc-in cofx [:interact/global-mouse-state :user-input :mouse :click-coords] click-coords)))

(rf/reg-event-fx
  :interact/palette-data
  (fn [cofx [_ key resource-data]]
    (assoc-in cofx [:data-view/global-view-state :data-display key] resource-data)))

(rf/reg-sub
  :data-view/register
  (fn []
    (@data-view :data-view)))

(rf/reg-sub
  :mouse-event/register
  (fn []
    (@mouse-events :interact)))

(rf/reg-sub
  :interact/start-location
  (fn []
    (rf/subscribe [:mouse-event/register]))
  (fn [register]
    (:click-coords (:mouse (:user-input register)))))

(rf/reg-event-fx
  :data-view/describe-statement
  (fn [cofx]
    (let [rdf-statement (mint/generate-statement)
          id (:ld.rdf/about rdf-statement)
          db (:db cofx)]
      {:data-view/global-view-state {:data-display {:resource-list  [id]
                                                    :statement-list [id]
                                                    :resources      {id {:position :statement
                                                                         :kind     :statement
                                                                         :value    nil}}
                                                    :active         {:statement id}}}
       :db                          (assoc-in db [:infopalette :rdf-statements] {id rdf-statement})})))

(rf/reg-event-fx
  :data-view/write-statement
  (fn []
    {:data-view/global-view-state {:data-display
                                   (into {}
                                         (for [k dd/resource-types]
                                           (let [v (k @(rf/subscribe [:store/rdf-contents]))]
                                             {v {:id    v
                                                 :type  k
                                                 :state :passive
                                                 :value nil}})))}}))

(rf/reg-cofx
  :data-view/global-data-view
  (fn [cofx]
    (let [data-display @(rf/subscribe [:data-view/register])]
      (assoc cofx :data-view/global-view-state data-display))))

(rf/reg-event-fx
  :data-view/show-resource
  [(rf/inject-cofx :data-view/global-data-view)]
  (fn [cofx [_ statement kind id]]
    (let [list (kind dd/kind-list)]
      (-> cofx
          (assoc-in [:data-view/global-view-state :data-display :resources statement :value kind] id)
          (assoc-in [:data-view/global-view-state :data-display :resources id] {:position   kind
                                                                                :kind       kind
                                                                                :statements [statement]
                                                                                :state      nil
                                                                                :value      nil})
          (update-in [:data-view/global-view-state :data-display :resource-list] (fns/conj-add id))
          (update-in [:data-view/global-view-state :data-display list] (fns/conj-add id))))))

(rf/reg-event-fx
  :data-view/activate
  [(rf/inject-cofx :data-view/global-data-view)]
  (fn [cofx [_ kind id]]
    (assoc-in cofx [:data-view/global-view-state :data-display :active kind] id)))

(rf/reg-sub
  :data-view/data-display
  (fn []
    (rf/subscribe [:data-view/register]))
  (fn [register]
    (get-in register [:data-display])))

(rf/reg-sub
  :data-view/statements
  (fn []
    (rf/subscribe [:data-view/data-display]))
  (fn [data-structure]
    (get-in data-structure [:statement-list])))

(rf/reg-sub
  :data-view/active
  (fn []
    (rf/subscribe [:data-view/data-display]))
  (fn [data-structure [_ type]]
    (get-in data-structure [:active type])))

(rf/reg-sub
  :data-view/statement
  (fn []
    (rf/subscribe [:store/rdf-contents]))
  (fn [data-structure [_ id]]
    (println "Dit kreeg ik binnen:" id data-structure)
    (get-in data-structure [id])))


; DONE: Convert keystrokes to letters (including space)
; DONE: Change letters to capitals when <<SHIFT>> is pressed
; DONE: Type letters in resource
; DONE: Show letters in active resource
; DONE: Store letters in local state
; TODO: Store terms in data store
; TODO: EXTRA - implement backspace
; TODO: EXTRA - implement arrow keys
; TODO: EXTRA - implement TAB - to switch to another resource
; TODO: EXTRA - implement something as a cursor within the text. So it's clear what location is active.