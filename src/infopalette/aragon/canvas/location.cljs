(ns infopalette.aragon.canvas.location
  (:require
    [infopalette.aragon.rdf.data-definition :as dd]
    [re-frame.core :as rf]
    [reagent.core :as r]
    [goog.events.EventType :as evt]))

(defonce env-details
         (let [a (r/atom {:canvas {:width  (.-innerWidth js/window)
                                   :height (.-innerHeight js/window)}})]
           (.addEventListener js/window evt/RESIZE
                              (fn [] (swap! a {:canvas {:width  (.-innerWidth js/window)
                                                        :height (.-innerHeight js/window)}})))
           a))

(rf/reg-sub
  :browser/properties
  (fn []
    @env-details))

(rf/reg-sub
  :browser/canvas-size
  (fn []
    (rf/subscribe [:browser/properties]))
  (fn [details]
    (:canvas details)))

(rf/reg-sub
  :coordinates/sizing
  (fn []
    [(rf/subscribe [:browser/canvas-size])
     (rf/subscribe [:interact/start-location])])
  (fn [[props locs]]
    (let [{:keys [width]} props
          {:keys [x y]} locs
          s-width (quot width 1.6)
          r-split (int (* s-width 0.07))
          txt-margin (int (* r-split 0.2))
          r-width (quot (- s-width (* r-split 2)) 3)
          r-height (int (* r-width 0.6))
          width-split (+ r-width r-split)
          group-x (- x (quot s-width 2))
          group-y (- y (quot r-height 2))]
      {:width       r-width
       :height      r-height
       :x           group-x
       :y           group-y
       :margin      txt-margin
       :x-increment width-split})))

(rf/reg-sub
  :coordinates/statement
  (fn []
    (rf/subscribe [:coordinates/sizing]))
  (fn [{:keys [:x :y]}]
    {:x x
     :y y}))

(rf/reg-sub
  :coordinates/resource-type
  (fn []
    (rf/subscribe [:coordinates/sizing]))
  (fn [{:keys [:x :y :width :height :margin :x-increment]} [_ resource-type]]
    (let [type-order (resource-type dd/type-order)
          new-x (+ x (* x-increment type-order))
          txt-x (+ new-x margin)
          txt-y (+ y (* 2 margin))]
      {:x      new-x
       :y      y
       :width  width
       :height height
       :txt-x  txt-x
       :txt-y  txt-y})))

