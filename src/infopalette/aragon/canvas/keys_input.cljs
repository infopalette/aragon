(ns infopalette.aragon.canvas.keys-input
  (:require [re-frame.core :as rf]
            [reagent.core :as r]
            [goog.events :as ev]
            [goog.events.EventType :as evt]
            [goog.events.KeyCodes :as evk]))

(defonce key-events (r/atom {:interact {:user-input {:keys {:recently []}}}}))

(rf/reg-sub
  :key-event/register
  (fn []
    (@key-events :interact)))

(rf/reg-sub
  :interact/keyboard-input
  (fn []
    (rf/subscribe [:key-event/register]))
  (fn [register]
    (:keys (:user-input register))))

(rf/reg-event-fx
  :interact/activate-keydown-listener
  (fn []
    {:interact/keydown-listener []}))

(comment "additional key properties as let bindings, for :interact/keydown-listener"
         "e-key (.-keyCode e)"
         "normalized (evk/normalizeKeyCode e-key)"
         "tag-name (.-tagName (.-target e))"
         "letter (str (char e-key))")

(rf/reg-fx
  :interact/keydown-listener
  (fn []
    (ev/listen
      js/document
      evt/KEYDOWN
      (fn [e]
        (let [hit-key {:alt-key   (.-altKey e)
                       :ctrl-key  (.-ctrlKey e)
                       :meta-key  (.-metaKey e)
                       :shift-key (.-shiftKey e)
                       :key-code  (.-keyCode e)}]
          (swap! key-events update-in [:interact :user-input :keys :recently] (fn [v]
                                                                                (let [keys (if hit-key
                                                                                             (conj (or v []) (into {:id (random-uuid)} (filterv (fn [[k v]]
                                                                                                                                 (and v [k v])) hit-key)))
                                                                                             v)]
                                                                                  (into []
                                                                                        (take-last 10 keys))))))))))

(rf/reg-sub
  :interact/typed-letter
  (fn []
    (rf/subscribe [:interact/keyboard-input]))
  (fn [k-input]
    #_(println ":interact/typed-letter" k-input)
    (let [key-combo (last (:recently k-input))
          kc (:key-code key-combo)
          id (:id key-combo)]
      [id (char (or (and (:shift key-combo) kc) (and (= kc 32) kc) (+ 32 kc)))])))

;;A =65
;;Z = 90