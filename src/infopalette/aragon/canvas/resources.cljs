(ns infopalette.aragon.canvas.resources
  (:require
    [infopalette.aragon.rdf.data-definition :as dd]
    [infopalette.aragon.canvas.interaction :as ci]
    [infopalette.aragon.canvas.location]
    [infopalette.aragon.canvas.keys-input]
    [infopalette.aragon.rdf.data-management :as rdm]
    [infopalette.aragon.defaults :as d]
    [infopalette.aragon.functions :as fns]
    [reagent.core :as r]
    [reagent.dom :as rdom]
    [re-frame.core :as rf]
    [clojure.string :as c-str]))

(defn text-in-tspan [txt map]
  (fn [txt map]
    (let [{:keys                        [x y width fill id]
           :or                          {x     0
                                         y     20
                                         width 50
                                         id    (str (random-uuid) ".default")
                                         fill  "black"}
           {:keys [font shape-inside text-align font-size]
            :or   {font         "sans-serif"
                   shape-inside "auto"
                   text-align   "left"
                   font-size    "1em"}} :style} map]
      (reduce
        (fn [coll i]
          (let [coll-str (last (last coll))
                full-str (str coll-str " " i)
                font-point-size 20
                col-y (:y ((last coll) 1))
                new-loc (+ col-y font-point-size)
                max-length (quot (* width 2) font-point-size)
                new-line? (<= max-length (count full-str))]
            (if new-line?
              (conj coll [:tspan {:x x :y new-loc} i])
              (conj (vec (butlast coll)) [:tspan {:x x :y col-y} full-str]))))
        [:text {:key   (fns/gen-key)
                :x     x
                :y     y
                :fill  fill
                :style {:font         font
                        :font-size    font-size
                        :shape-inside shape-inside
                        :text-align   text-align}} [:tspan {:x x
                                                            :y y} ""]]
        (c-str/split txt #" ")))))

(defn rectangle
  ([map]
   (rectangle nil map))
  ([atom map]
   (let [{:keys [width height x y fill id]
          :or   {width  50
                 height 50
                 x      104
                 y      104
                 id     (str (random-uuid) ".default")
                 fill   :aquamarine}
          :as   all} map
         settings (if (and atom (= (get-in @atom [:resources :active]) (re-find #"^[^.]+" id)))
                    (assoc-in all [:style] {:stroke-width 5
                                            :stroke       :blue})
                    all)]
     (println "het atoom:" atom "de instellingen:" settings)
     [:rect settings])))


(defn match-predicate [pred]
  (fn [[p _]]
    (= pred p)))

(defn match-predicate-set [pset]
  (fn [[p _]]
    (pset p)))

(defn resource [statement type id]
  (let [s (r/atom {:id       id
                   :k-id     nil
                   :contents []})]
    (rf/dispatch-sync [:data-view/show-resource statement type id])
    (fn [_ type id]
      (let [{:keys [:width :height :x :y :txt-x :txt-y]} @(rf/subscribe [:coordinates/resource-type type])
            id (:id @s)
            active (= id @(rf/subscribe [:data-view/active :resource]))
            [k-id c] @(rf/subscribe [:interact/typed-letter])]
        (when active
          (when (not= k-id (:k-id @s))
            (swap! s update :contents conj c)))
        (swap! s assoc :k-id k-id)
        [:g
         [:rect {:fill         :aquamarine
                 :width        width
                 :height       height
                 :stroke-width (if active
                                 3
                                 0)
                 :stroke       :firebrick
                 :x            x
                 :y            y
                 :on-click     #(rf/dispatch [:data-view/activate :resource id])}]
         [:text {:fill  :black
                 :style d/font-settings}
          [:tspan {:x   txt-x
                   :y   txt-y}
           (apply str (:contents @s))]]]))))

(defn display-statement []
  (let [s (r/atom {:id nil})]
    (rf/dispatch-sync [:data-view/describe-statement])
    (fn []
      (let [location @(rf/subscribe [:coordinates/statement])
            statement @(rf/subscribe [:data-view/active :statement])
            resources @(rf/subscribe [:data-view/statement statement])]
        (when (not (:id @s)) (swap! s assoc :id statement))
        [:g location
         (println "Zo zien de resources er uit: " resources)
         (doall
           (for [type dd/resource-types]
             (let [switch (type dd/type-switcher)]
               (println "het type is:" type)
               ^{:key (fns/gen-key)} [resource statement switch (type resources)])))]))))

(defn palette []
  [display-statement])
