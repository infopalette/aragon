(ns infopalette.aragon.debug
  (:require [goog.object :as gobject]
            [re-frame.core :as rf]
            [reagent.core :as r]
            [reagent.dom :as rdom]))

(def debug true)

(defn js-obj->clj-map
  [obj]
  (zipmap (gobject/getKeys obj) (gobject/getValues obj)))

(defn dbg-show-db []
  (let [d (rf/subscribe [:store/contents])]
    (r/create-class
      {:display-name        "tekst-blokje"
       :component-did-mount (fn [comp]
                              (do (println "alleen het tekst-blokje")
                                  (.dir js/console (rdom/dom-node comp))
                                  (println "is dit de tekst lengte: " (-> (rdom/dom-node comp)
                                                                          (.-lastChild)
                                                                          (.-lastChild)))))
       :reagent-render      (fn []
                              [:text#blokje {:x 0
                                             :y 20}
                               [:tspan#spannend1 {:style {:font         "14px sans-serif"
                                                          :shape-inside "auto"
                                                          :text-align   "left"}}
                                [pr-str "db: " @d]]])})))


;
; DONE: Have empty app/DB at initialization
; DONE: Generate statement
;   DONE: Use keywords for uri's in statement
;   DONE: Add statement to app/DB
;   DONE: Refer to statement in user-interaction r/atom.
; DONE: Create subscribtions with coordinates for statements and subject, predicate, object.
; DONE: Change border color for active resource