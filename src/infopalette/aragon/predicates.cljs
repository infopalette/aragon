(ns infopalette.aragon.predicates)

(defn variable?
  [s]
  (and (symbol? s) (.startsWith (name s) "?")))

(defn uri-str?
  [o]
  (and (string? o) (re-matches #"^<.*>$" o)))

(defn literal?
  [obj]
  (not (coll? obj)))