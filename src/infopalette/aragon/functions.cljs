(ns infopalette.aragon.functions)

(defn conj-add [id]
  (fn [array]
    (if array
      (conj array id)
      [id])))

(defn gen-key []
  (gensym "key-"))