(ns infopalette.aragon.rdf.resolver)


(def prefixes {:ld.ipi "https://ld.infopalette.org/id/"
               :ld.ipv "https://ld.infopalette.org/voc/"})

(def session-base :ld.ipi)