(ns infopalette.aragon.rdf.data-definition)

(defonce predicate-filter #{:ld.rdf/about :ld.rdf/type :ld.rdf/subject :ld.rdf/predicate :ld.rdf/object})

(defonce type-filter #{:ld.rdf/type})

(defonce subject-filter #{:ld.rdf/about})

(defonce resource-types [:ld.rdf/subject :ld.rdf/predicate :ld.rdf/object])

(defonce type-switcher {:ld.rdf/subject   :subject
                        :ld.rdf/predicate :predicate
                        :ld.rdf/object    :object})

(defonce type-order {:subject 0
                     :predicate 1
                     :object 2})

(defonce kind-list {:subject :subject-list
                    :predicate :predicate-list
                    :object :object-list})