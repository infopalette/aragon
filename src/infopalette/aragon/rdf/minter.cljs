(ns infopalette.aragon.rdf.minter
  (:require [infopalette.aragon.rdf.resolver :as reslv]))

(defn mint-uri
  "Creates iri instances by adding an UUID to a base IRI"
  ([base-iri]
   (let [uuid (random-uuid)]
     (keyword base-iri (str "uuid-" uuid))))
  ([base-iri amount]
   (vec (repeatedly amount (keyword base-iri (random-uuid))))))

(defn generate-statement
  "Creates a RDF statement, with subject, predicate and object.
  This implies new IRI's. Applying a base IRI to four UUID's."
  []
  (let [base-uri reslv/session-base]
    {:ld.rdf/about (mint-uri base-uri)
     :ld.rdf/type :ld.rdf/Statement
     :ld.rdf/subject (mint-uri base-uri)
     :ld.rdf/predicate (mint-uri base-uri)
     :ld.rdf/object (mint-uri base-uri)}))
