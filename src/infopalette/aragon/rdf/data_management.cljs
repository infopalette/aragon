(ns infopalette.aragon.rdf.data-management
  (:require [re-frame.core :as rf]
            [infopalette.aragon.rdf.minter :as mint]))

(rf/reg-event-db
  :app/start-up
  [re-frame.core/debug]
  (fn [db _]
    (assoc db :infopalette {:components     {}
                            :rdf-statements {}})))

(rf/reg-sub
  :store/contents
  (fn [db]
    db))

(rf/reg-sub
  :store/rdf-contents
  (fn [db]
    (get-in db [:infopalette :rdf-statements])))

(defn find-nested
  [m k]
  (->> (tree-seq map? vals m)
       (filter map?)
       (some k)))