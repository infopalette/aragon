(ns infopalette.spec
  (:require [clojure.spec.alpha :as s]
            [infopalette.aragon.predicates :as p]))

(s/def ::quad
      (s/cat :op #{:quad} :quad ::quad))

(s/def ::quad-block
      (s/cat :op #{:quad-block} :quads (s/+ ::quad)))

(s/def ::quad-pattern
      (s/cat :op #{:quad-pattern}
             :graph-id ::node
             :triples (s/+ ::triples)))

(s/def ::variable p/variable?)

(s/def ::iri (s/or :keyword keyword?
                   :uri p/uri-str?))

(s/def ::literal p/literal?)

(s/def ::named-blank #(and (symbol? %) (.startsWith (name %) "_")))
(s/def ::anon-blank #(= '_ %))

(s/def ::blank (s/or :anonymous ::anon-blank
                     :named ::named-blank))

(s/def ::node (s/or :variable ::variable
                    :blank    ::blank
                    :iri      ::iri
                    :literal  ::literal))

(s/def ::triple (s/tuple ::node ::node ::node))

(s/def ::triples (s/or :map map?
                       :maps (s/coll-of map? :min-count 1)
                       :triples (s/coll-of ::triple :min-count 1)
                       :single-triple ::triple
                       :graph ::quad
                       :empty #(and (coll? %) (empty? %))))